package Encrypt;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.didisoft.pgp.PGPException;
import com.didisoft.pgp.PGPKeyPair;
import com.didisoft.pgp.PGPLib;

public class EncryptString {
	
	
	public String Encrypt(String stringEncrypt) throws PGPException, IOException {
		
	
		String encryptedString = null;
		String encryptedFinal = "";
		
		PGPLib pgp = new PGPLib();
		//cifrado.asc archivo con llave publica 
		String filePublicKey = "cifrado.asc";
		PGPKeyPair  key = new PGPKeyPair ("cifrado.asc");
		
		
		InputStream publicEncryptStream = null;
		
		try {
			publicEncryptStream = new FileInputStream(filePublicKey);
			
	        encryptedString = pgp.encryptString(stringEncrypt, publicEncryptStream);
	        
	        
	        if(!encryptedString.isEmpty()) {
	        String formatEncrypted [] = encryptedString.split("\\n");
	        
	        for (String stringFormat : formatEncrypted) {
				if(validate(stringFormat)) {
					encryptedFinal += stringFormat.substring(0,stringFormat.length()-1);
				}
			}
	        }
		}catch(com.didisoft.pgp.PGPException e) {
			e.getStackTrace();
		}
		finally {
            if (publicEncryptStream != null)
            	publicEncryptStream.close();
		}
		
		return encryptedFinal;
	}

	private boolean validate(String stringFormat) {
		if(stringFormat.contains("Version")||stringFormat.contains("-----")) {
			return false;
		}
		return true;
	}
}
