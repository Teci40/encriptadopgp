package Encrypt;

import java.io.IOException;

import com.didisoft.pgp.PGPException;
import com.didisoft.pgp.PGPLib;

class DecryptString{

	
	public String decrypt(String pathFile) throws PGPException, IOException {
		PGPLib pgp = new PGPLib();
		String openpgpMessage = "cifrado.txt";
		String privateKeyFile  = "private.asc";
		String privateKeyPassword = " ";
		
        String decryptedMessage = pgp.decryptString(openpgpMessage, privateKeyFile,privateKeyPassword);
        
		return decryptedMessage;
	}
}
